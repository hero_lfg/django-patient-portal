from __future__ import unicode_literals

from django.apps import AppConfig


class HerolfgAuthConfig(AppConfig):
    name = 'herolfg_auth'
