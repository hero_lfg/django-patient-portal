# Django Patient Portal #

https://herolfg.com/information-technology-healthcare-problem/


### Engineer Audit (automation tool op) ###

* create a cloud9 workspace based on git@bitbucket.org:hero_lfg/django-patient-portal.git
* virtualenv .django-patient-portal
* source .django-patient-portal
* touch .gitignore
* add .django-patient-portal directory to .gitignore
* pip install django
* pip install djangorestframework
* django-admin startproject herolfg_patient_portal
* cp herolfg_patient_portal/settings.py herolfg_patient_portal/base_settings.py
* sensitive data should be removed from base_settings and kept in settings
* settings should include base_settings and can be updated in secret for deployments
* add settings.py to .gitignore
* cd herolfg_patient_portal
* django-admin startapp herolfg_auth
* touch herolfg_auth/viewsets.py
* touch herolfg_auth/serializers.py
* add auth source code from DRF http://www.django-rest-framework.org/tutorial/quickstart/
* I choose to use viewsets.py instead of views.py
* make sure to update herolfg_patient_portal/urls.py with urls.py from DRF quickstart
* add "rest_framework" and "herolfg_auth" to herolfg_patient_portal/settings.py INSTALLED_APPS
* ./manage.py migrate
* ./manage.py createsuperuser
